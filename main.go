package main

import (
	"fmt"
	"image"
	"os"
	"time"

	"github.com/faiface/pixel"
	"github.com/faiface/pixel/pixelgl"
	colors "golang.org/x/image/colornames"

	_ "image/png"
)

// Tile is a type of the tile to place on the tile map.
type Tile int

// Tile types.
const (
	Wall             Tile = 0
	Floor            Tile = 1
	None             Tile = 2
	Roof             Tile = 3
	Door             Tile = 4
	Passage          Tile = 5
	LeftRoof         Tile = 6
	RightRoof        Tile = 7
	ReverseLeftRoof  Tile = 8
	ReverseRightRoof Tile = 9
	LeftRoofEnd      Tile = 10
	RightRoofEnd     Tile = 11
)

const (
	tileSize = 64
	width    = 1280
	height   = 720
)

func loadPicture(pic string) (pixel.Picture, error) {
	file, err := os.Open(pic)

	if err != nil {
		return nil, err
	}
	defer file.Close()

	img, _, err := image.Decode(file)

	if err != nil {
		return nil, err
	}

	return pixel.PictureDataFromImage(img), nil
}

func getDiagonals(tilemap [][]Tile) [][]Tile {
	length := len(tilemap)
	diagonals := make([][]Tile, 0)

	for i := 0; i < length-1; i++ {
		diagonals = append(diagonals, make([]Tile, 0))

		for j := i; j >= 0; j-- {
			diagonals[i] = append(diagonals[i], tilemap[j][i-j])
		}
	}

	sideDiagonal := make([]Tile, 0)

	for i := length - 1; i >= 0; i-- {
		sideDiagonal = append(sideDiagonal, tilemap[i][length-i-1])
	}

	diagonals = append(diagonals, sideDiagonal)

	for i := length - 2; i >= 0; i-- {
		arr := make([]Tile, 0)

		for j := 0; j <= i; j++ {
			arr = append(arr, tilemap[length-j-1][length+j-i-1])
		}

		diagonals = append(diagonals, arr)
	}

	return diagonals
}

func run() {
	// Create a new window.
	cfg := pixelgl.WindowConfig{
		Title:  "Isometric tilemap demo",
		Bounds: pixel.R(0, 0, 1280, 720),
		//VSync:  true,
		//Undecorated: true,
		//Monitor: pixelgl.PrimaryMonitor(),
	}
	win, err := pixelgl.NewWindow(cfg)
	handleError(err)

	// Load tile sheet.
	tilesheet, err := loadPicture("castle.png")
	handleError(err)

	offset := pixel.V(360, 960)
	tilemap := [][][]Tile{
		{
			{0, 0, 0, 0, 0, 0, 0},
			{0, 1, 1, 1, 1, 1, 0},
			{0, 1, 1, 1, 1, 1, 0},
			{0, 1, 1, 4, 1, 1, 0},
			{0, 1, 1, 1, 1, 1, 0},
			{0, 1, 1, 1, 1, 1, 0},
			{0, 0, 0, 5, 0, 0, 0},
		},
		{
			{2, 9, 9, 9, 9, 9, 11},
			{8, 2, 2, 2, 2, 2, 7},
			{8, 2, 2, 2, 2, 2, 7},
			{8, 2, 2, 0, 2, 2, 7},
			{8, 2, 2, 2, 2, 2, 7},
			{8, 2, 2, 2, 2, 2, 7},
			{10, 6, 6, 6, 6, 6, 3},
		},
		{
			{2, 2, 2, 2, 2, 2, 2},
			{2, 2, 2, 2, 2, 2, 2},
			{2, 2, 2, 2, 2, 2, 2},
			{2, 2, 2, 3, 2, 2, 2},
			{2, 2, 2, 2, 2, 2, 2},
			{2, 2, 2, 2, 2, 2, 2},
			{2, 2, 2, 2, 2, 2, 2},
		},
	}
	levels := make([][][]Tile, 0)

	for _, level := range tilemap {
		levels = append(levels, getDiagonals(level))
	}

	sprites := make([]*pixel.Sprite, 0)
	transforms := make([]pixel.Matrix, 0)

	for i := range levels {
		for j := range levels[i] {
			length := len(levels[i][j])
			localOffset := pixel.V(offset.X-float64(length-1)*tileSize/2, offset.Y+float64(i)*tileSize/2-float64(j)*tileSize/4)

			for k := range levels[i][j] {
				switch levels[i][j][k] {
				case Wall:
					wallTile := pixel.NewSprite(tilesheet, pixel.R(0, 448, tileSize, 512))
					sprites = append(sprites, wallTile)

				case Floor:
					floorTile := pixel.NewSprite(tilesheet, pixel.R(0, 128, tileSize, 192))
					sprites = append(sprites, floorTile)

				case Roof:
					roofTile := pixel.NewSprite(tilesheet, pixel.R(576, 320, 640, 384))
					sprites = append(sprites, roofTile)

				case Door:
					doorTile := pixel.NewSprite(tilesheet, pixel.R(576, 256, 640, 320))
					sprites = append(sprites, doorTile)

				case Passage:
					passageTile := pixel.NewSprite(tilesheet, pixel.R(576, 192, 640, 256))
					sprites = append(sprites, passageTile)

				case LeftRoof:
					leftRoofTile := pixel.NewSprite(tilesheet, pixel.R(192, 384, 256, 450))
					sprites = append(sprites, leftRoofTile)

				case RightRoof:
					rightRoofTile := pixel.NewSprite(tilesheet, pixel.R(256, 384, 320, 450))
					sprites = append(sprites, rightRoofTile)

				case ReverseLeftRoof:
					reverseLeftRoofTile := pixel.NewSprite(tilesheet, pixel.R(320, 384, 384, 448))
					sprites = append(sprites, reverseLeftRoofTile)

				case ReverseRightRoof:
					reverseRightRoofTile := pixel.NewSprite(tilesheet, pixel.R(448, 384, 512, 448))
					sprites = append(sprites, reverseRightRoofTile)

				case LeftRoofEnd:
					leftRoofEndTile := pixel.NewSprite(tilesheet, pixel.R(128, 64, 192, 128))
					sprites = append(sprites, leftRoofEndTile)

				case RightRoofEnd:
					rightRoofEndTile := pixel.NewSprite(tilesheet, pixel.R(192, 64, 256, 128))
					sprites = append(sprites, rightRoofEndTile)

				default:
					continue
				}

				transform := pixel.IM.Moved(pixel.V(localOffset.X+float64(k)*tileSize, localOffset.Y))
				transforms = append(transforms, transform)
			}
		}
	}

	// Camera properties.
	cameraPosition := offset
	cameraSpeed := 500.0

	last := time.Now()
	fps := 0
	perSecond := time.Tick(time.Second)

	for !win.Closed() {
		deltaTime := time.Since(last).Seconds()
		last = time.Now()

		win.Clear(colors.White)

		// Adjust camera transform.
		camera := pixel.IM.
			Moved(win.Bounds().Center().Sub(cameraPosition))
		win.SetMatrix(camera)

		for i := range sprites {
			sprites[i].Draw(win, transforms[i])
		}

		// Handle camera moving.
		if win.Pressed(pixelgl.KeyLeft) {
			cameraPosition.X -= cameraSpeed * deltaTime
		}

		if win.Pressed(pixelgl.KeyRight) {
			cameraPosition.X += cameraSpeed * deltaTime
		}

		if win.Pressed(pixelgl.KeyDown) {
			cameraPosition.Y -= cameraSpeed * deltaTime
		}

		if win.Pressed(pixelgl.KeyUp) {
			cameraPosition.Y += cameraSpeed * deltaTime
		}

		win.Update()

		// Show FPS in the window title.
		fps++

		select {
		case <-perSecond:
			win.SetTitle(fmt.Sprintf("%s | FPS: %d", cfg.Title, fps))
			fps = 0

		default:
		}
	}
}

func main() {
	pixelgl.Run(run)
}

func handleError(err error) {
	if err != nil {
		panic(err)
	}
}
